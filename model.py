#from datetime import datetime

import pandas as pd
import numpy as np


def equal_name(name1,name2): # returns whether 2 halfnames (fn or ln) are equal or not
	name1, name2 = name1.split(), name2.split()
	for i in name1:
		if ((i in name2) and (len(i) > 2)): # here (len(i) > 2) represents avoidance of 'JR' and similar name attributes/constituents
				return(True)
	return(False)

def equal_entry(entry1, entry2): # returns whether 2 rows of data base are equal or not
	if ((entry1['dob'] is entry2['dob'])
		and
		(entry1['gn'] is entry2['gn'])
		and
		equal_name(entry1['fn'], entry2['fn'])
		and
		equal_name(entry1['ln'], entry2['ln'])
	   ):
		return(True)
	else:
		return(False)

def deduplicate_index(data): # returns a boolean numpy array with False for entries(/rows) which already appeared before
	check = np.ones(data.shape[0], dtype=bool)
	# iterative (brute force) approach for finding duplicates
	# no other way possible with 100% surety, given the possible defined variations
	# divide and conquer can be applied
	for i in range(0,data.shape[0]-1):
		for j in range(i+1,data.shape[0]):
			if (check[j]):
				if (equal_entry(data.iloc[i],data.iloc[j])):
					check[j] = False
	return(check)

def deduplicate(data):
	data = data[deduplicate_index(data)]
	# data.set_index(np.arange(data.shape[0]), inplace=True)
	return(data)

def efficient_deduplicate(data): # use the method of divide and conquer to minimise the load of brute force computations
	if data.shape[0] < 10:
		return(deduplicate(data))
	else:
		mid = data.shape[0]//2
		d1, d2 = data.iloc[:mid], data.iloc[mid:]
		d1, d2 = efficient_deduplicate(d1), efficient_deduplicate(d2)
		d = deduplicate(pd.concat([d1, d2]))
		# d.set_index(np.arange(data.shape[0]), inplace=True)
		return(d)

def data_handling(filein, fileout):
	data = pd.read_csv(filein)
	# data = deduplicate(data)
	data = efficient_deduplicate(data)
	# data.set_index(np.arange(data.shape[0]), inplace=True)
	data.to_csv(fileout, index=False)


if __name__ == '__main__':
	filein = 'Deduplication Problem - Sample Dataset.csv' # file to be read
	fileout = 'out.csv' # file to be written comprising of the deduplicated data
	
	# startTime = datetime.now()

	data_handling(filein, fileout)

	# print('brute', datetime.now() - startTime) # brute 0:00:00.981575
	# print('eff', datetime.now() - startTime) # eff 0:00:00.614767
