# Innovaccer HackerCamp Summer 2018 Submission - Analytics

In the python3 script **model.py**, the variable **filein** stores the name & address of .csv file containing the data to be de-duplicated, while the variable **fileout** stores the name & address of .csv file to be written containing the de-duplicated data.

### Note : The input file should contain data in the format specified as seperated into columns with title 'fn', 'ln', 'gn' & 'dob' representing First-name, Last-name, Gender & Date of Birth, respectively.